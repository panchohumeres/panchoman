.. Linux documentation master file, created by
   sphinx-quickstart on Sat Oct 12 03:05:57 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Manual Linux de Francisco Humeres M. (c) 2020
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   1. Comandos Útiles <../_sections/useful-commands>


