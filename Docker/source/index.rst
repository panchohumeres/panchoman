.. Docker documentation master file, created by
   sphinx-quickstart on Sat Oct 12 03:05:57 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Manual Docker de Francisco Humeres M. (c) 2020
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   1. Generales <../_sections/general>
   2. Revisar Logs de Contenedores <../_sections/logs>
   3. Borrar, Limpiar Contenedores <../_sections/prune>


