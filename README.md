# panchoMan
Manuales varios de Francisco Humeres M.

[Linux](https://panchohumeres.gitlab.io/linux_man/)

[Docker](https://panchohumeres.gitlab.io/docker_man_page/)

[Sphinx](/sphinx/sphinx.md)

[Git](/sphinx/git.md)